﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] int max;
    [SerializeField] int min;
    [SerializeField] TextMeshProUGUI guessText;

    int guess;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        MakeGuess();
    }

    public void OnGuessHigher()
    {
        min = guess + 1;
        MakeGuess();
    }

    public void OnGuessLower()
    {
        max = guess - 1;
        MakeGuess();
    }

    void MakeGuess()
    {
        guess = Random.Range(min, max + 1);
        guessText.text = guess.ToString();
    }
}
